const express = require("express")
const app = express()
const port = 3000
const bodyParser = require("body-parser")
app.use(bodyParser.json())

const createRoleOperation = require("./operation/create.js")

app.post("/create", (req, res) => {
	createRoleOperation(req.body.organisation, req.body.role, (result, statusCode) => {
		res.statusCode = statusCode
		res.send(result)
	})
})

app.listen(port, () => {
	console.log("role service started, listening on port: " + port)
})

