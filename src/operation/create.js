const axios = require("axios")
const config = require("../config.json")

module.exports = function(organisation, role, callback) {
    let name = role.name
    let services = role.services
	axios.post(config.mongoService + "/insert-one", {
		"database": organisation,
		"collection": config.collection,
        "data": {
            "name": name,
            "services": services
        }
	}).then((response) => {
        axios.post(config.mongoService  + "/find-one", {
            
        })
		callback(response.data, 200)
	}).catch((err) => {
		console.log(err)
		callback(err, 500)
	})
}

